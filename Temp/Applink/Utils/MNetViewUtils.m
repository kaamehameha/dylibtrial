//
//  MNetViewUtils.m
//  Pods
//
//  Created by kunal.ch on 15/05/17.
//
//

#import "MNetViewUtils.h"
#import <objc/runtime.h>

@implementation MNetViewUtils

+(BOOL) isAdapterChild:(UIView *)view viewController:(UIViewController *)controller {
    return ([controller isKindOfClass:[UITableViewController class]] ||
            [controller isKindOfClass:[UICollectionViewController class]] ||
            [controller isKindOfClass:[UITabBarController class]]
            );
}

/*
  Resource name = "(Name of view)" + ":" + "index/" + "(index of view in superview)"
  Temporary way to give resource name.
 */

+(NSString *) getResourceNameForView:(UIView *)view withId:(int)viewId {
    UIView *parentView = [view superview];
    NSUInteger index = [parentView.subviews indexOfObject: view];
    NSString *resourceName = [NSStringFromClass(view.class) lowercaseString];
    return [resourceName stringByAppendingString:[NSString stringWithFormat:@":index/%lu", index]];
}

+(void) swizzleMethod:(SEL)originalSel
     withSwizzlingSel:(SEL)swizzledSel
            fromClass:(Class)className {
        
        Method originalMethod = class_getInstanceMethod(className, originalSel);
        Method swizzledMethod = class_getInstanceMethod(className, swizzledSel);
        
        BOOL didAddMethod = class_addMethod(className,
                                            originalSel,
                                            method_getImplementation(swizzledMethod),
                                            method_getTypeEncoding(swizzledMethod));
        
        
        if (didAddMethod) {
                class_replaceMethod(className,
                                    swizzledSel,
                                    method_getImplementation(originalMethod),
                                    method_getTypeEncoding(originalMethod));
        } else {
                method_exchangeImplementations(originalMethod, swizzledMethod);
        }
}
@end
