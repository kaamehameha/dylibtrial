//
//  MNetLinkUtils.h
//  Pods
//
//  Created by kunal.ch on 16/05/17.
//
//

#import <Foundation/Foundation.h>

@interface MNetLinkUtils : NSObject


/*
 * Generates unique link for controller name
 */
+(NSString *) getURIForControllerName : (NSString *) controllerName;

@end
