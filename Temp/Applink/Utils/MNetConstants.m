//
//  MNetConstants.m
//  Pods
//
//  Created by kunal.ch on 16/05/17.
//
//

#import "MNetConstants.h"

NSString *const TEXT = @"text";
NSString *const VIEW_CLASS = @"view_class";
NSString *const VIEW_TYPE = @"view_type";
NSString *const SCROLLABLE = @"scrollable";
NSString *const VIEW_ID = @"view_id";
NSString *const RESOURCE_NAME = @"resource_name";
NSString *const CLICKABLE = @"clickable";
NSString *const PROPERTIES = @"properties";
NSString *const PARENT = @"parent";
NSString *const CHILD = @"child";
NSString *const PAGE_COUNT = @"page_count";
NSString *const START_OFFSET = @"start_offset";
NSString *const SEGMENTS = @"segments";

@implementation MNetConstants

@end
