//
//  MNetViewUtils.h
//  Pods
//
//  Created by kunal.ch on 15/05/17.
//
//

#import <Foundation/Foundation.h>

@interface MNetViewUtils : NSObject

+(BOOL) isAdapterChild : (UIView *) view viewController : (UIViewController *) controller;

/*
 Resource name = "(Name of view)" + ":" + "index/" + "(index of view in superview)"
 Temporary way to give resource name.
 */

+(NSString *) getResourceNameForView : (UIView *) view withId : (int) viewId;

+(void) swizzleMethod:(SEL)originalSel
     withSwizzlingSel:(SEL)swizzledSel
            fromClass:(Class)className;

@end
