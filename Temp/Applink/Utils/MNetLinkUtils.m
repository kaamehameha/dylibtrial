//
//  MNetLinkUtils.m
//  Pods
//
//  Created by kunal.ch on 16/05/17.
//
//

#import "MNetLinkUtils.h"

@implementation MNetLinkUtils

+(NSString *) getURIForControllerName:(NSString *)controllerName{
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    NSArray *bundleIdentifierArray = [bundleIdentifier componentsSeparatedByString:@"."];
    NSString *identifier = @"";
    NSUInteger i = [bundleIdentifierArray count];
    for(; i > 0; i--){
        identifier = [identifier stringByAppendingString:bundleIdentifierArray[i-1]];
        if((i-1) != 0){
            identifier = [identifier stringByAppendingString:@"."];
        }
    }
    NSString *appBuildNumber = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSArray *buildNumberArray = [appBuildNumber componentsSeparatedByString:@"."];
    NSString *http = @"http://";
    NSString *url = [http stringByAppendingString:[identifier lowercaseString]];
    return [url stringByAppendingString:[NSString stringWithFormat:@".imnapp/%@/%@", buildNumberArray[0], controllerName]];
}
@end
