//
//  MNetConstants.h
//  Pods
//
//  Created by kunal.ch on 16/05/17.
//
//

#import <Foundation/Foundation.h>

extern NSString *const Text;
extern NSString *const VIEW_CLASS;
extern NSString *const VIEW_TYPE;
extern NSString *const RESOURCE_NAME;
extern NSString *const VIEW_ID;
extern NSString *const SCROLLABLE;
extern NSString *const CLICKABLE;
extern NSString *const PROPERTIES;
extern NSString *const PARENT;
extern NSString *const CHILD;
extern NSString *const START_OFFSET;
extern NSString *const PAGE_COUNT;
extern NSString *const SEGMENTS;

@interface MNetConstants : NSObject

@end
