//
//  MNetViewControllerParser.m
//  Pods
//
//  Created by kunal.ch on 05/05/17.
//
//

#import "MNetViewControllerParser.h"
#import <WebKit/WebKit.h>

@interface MNetViewControllerParser ()
// viewcontroller under parsing
@property(nonatomic) UIViewController *viewController;
// dictionary to maintain viewcontroller and its view heirarchy
@property(nonatomic) NSMutableDictionary *viewControllerParseMap;

@end

@implementation MNetViewControllerParser

- (instancetype)init {
  self = [super init];
  if (self) {
    _viewControllerParseMap = [[NSMutableDictionary alloc] init];
  }
  return self;
}

+ (MNetViewControllerParser *)sharedInstance {
  static MNetViewControllerParser *sharedInstance;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedInstance = [[self alloc] init];
  });
  return sharedInstance;
}

- (void)startParsingViewController:(UIViewController *)viewController {
  _viewController = viewController;
  NSMutableDictionary *newViewControllerTree =
      [[NSMutableDictionary alloc] init];
  [newViewControllerTree setObject:[self generateViewTree:viewController.view
                                           viewController:viewController]
                            forKey:@"View"];
  NSLog(@"parsed dictionary is for viewcontroller : %@ is %@",
        NSStringFromClass(viewController.class), newViewControllerTree);
}

- (NSMutableArray *)generateViewTree:(UIView *)view
                      viewController:(UIViewController *)controller {

  NSMutableArray *subViewArray = [[NSMutableArray alloc] init];

  for (UIView *subview in view.subviews) {

    if ([subview isKindOfClass:[UITextView class]]) {

      UITextView *textView = (UITextView *)subview;

      if (textView.text != nil) {

        NSMutableDictionary *leaf = [[NSMutableDictionary alloc] init];

        [leaf setObject:textView.text forKey:@"TV"];

        [subViewArray addObject:leaf];
      }

    } else if ([subview isKindOfClass:[UILabel class]]) {

      UILabel *label = (UILabel *)subview;

      if (label.text != nil) {

        NSMutableDictionary *leaf = [[NSMutableDictionary alloc] init];

        [leaf setObject:label.text forKey:@"L"];

        [subViewArray addObject:leaf];
      }

    } else if ([subview isKindOfClass:[UIButton class]]) {

      UIButton *button = (UIButton *)subview;

    } else if ([subview isKindOfClass:[UITextField class]]) {

      UITextField *textField = (UITextField *)subview;

      if (textField.text != nil) {

        NSMutableDictionary *leaf = [[NSMutableDictionary alloc] init];

        [leaf setObject:textField.text forKey:@"TF"];

        [subViewArray addObject:leaf];
      }

    } else if ([subview isKindOfClass:[UITableView class]]) {

      NSMutableDictionary *leaf = [[NSMutableDictionary alloc] init];

      [leaf setObject:[self parseTableView:(UITableView *)subview
                            viewController:controller]
               forKey:@"List"];

      [subViewArray addObject:leaf];

    } else if ([subview isKindOfClass:[UICollectionView class]]) {

      NSMutableDictionary *leaf = [[NSMutableDictionary alloc] init];

      [leaf setObject:[self parseCollectionView:(UICollectionView *)subview
                                 viewController:controller]
               forKey:@"CollectionList"];

      [subViewArray addObject:leaf];

    } else if ([subview isKindOfClass:[WKWebView class]]) {

      WKWebView *webView = (WKWebView *)subview;
      NSMutableDictionary *leaf = [[NSMutableDictionary alloc] init];
      if (webView.URL.absoluteString != nil) {
        [leaf setObject:webView.URL.absoluteString forKey:@"webview"];
        [subViewArray addObject:leaf];
      }

    } else {

      if ([subview.subviews count] > 0) {
        NSArray *arrayTree =
            [self generateViewTree:subview viewController:controller];
        if ([arrayTree count] != 0) {
          NSMutableDictionary *subNode = [[NSMutableDictionary alloc] init];
          [subNode setObject:[self generateViewTree:subview
                                     viewController:controller]
                      forKey:@"SubView"];
          [subViewArray addObject:subNode];
        }
      }
    }
  }
  return subViewArray;
}

- (NSMutableArray *)parseTableView:(UITableView *)tableView
                    viewController:(UIViewController *)controller {

  // This needs to update right now only parsing the visible cells just to test
  // the parsing logic
  NSMutableArray *tableViewTree = [[NSMutableArray alloc] init];

  for (UITableViewCell *visibleCells in tableView.visibleCells) {
    [tableViewTree addObject:[self generateViewTree:visibleCells.contentView
                                     viewController:controller]];
  }
  return tableViewTree;
}

- (NSMutableArray *)parseCollectionView:(UICollectionView *)collectionView
                         viewController:(UIViewController *)controller {
  NSMutableArray *collectionViewTree = [[NSMutableArray alloc] init];
  for (UICollectionViewCell *visibleCells in collectionView.visibleCells) {
    [collectionViewTree
        addObject:[self generateViewTree:visibleCells.contentView
                          viewController:controller]];
  }
  return collectionViewTree;
}

- (void)stopParsingViewController:(UIViewController *)viewController {
}

@end
