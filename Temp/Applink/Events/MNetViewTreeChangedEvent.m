//
//  MNetViewTreeChangedEvent.m
//  Pods
//
//  Created by kunal.ch on 16/05/17.
//
//

#import "MNetViewTreeChangedEvent.h"
#import "MNetLinkUtils.h"

@interface MNetViewTreeChangedEvent()

@property(nonatomic) NSString *link;
@end
@implementation MNetViewTreeChangedEvent

-(void) process {
    
}

-(NSString *) getLinkForViewController:(NSString *)controllerName {
    NSString *link = [MNetLinkUtils getURIForControllerName:controllerName];
    _link = link;
    return link;
}

@end
