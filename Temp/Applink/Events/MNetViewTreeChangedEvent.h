//
//  MNetViewTreeChangedEvent.h
//  Pods
//
//  Created by kunal.ch on 16/05/17.
//
//

#import <Foundation/Foundation.h>

@interface MNetViewTreeChangedEvent : NSObject

-(void) process;
-(NSString *) getLinkForViewController : (NSString *) controllerName;

@end
