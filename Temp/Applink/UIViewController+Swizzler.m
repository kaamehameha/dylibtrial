//
//  UIViewController+Swizzler.m
//  Pods
//
//  Created by kunal.ch on 05/05/17.
//
//

#import "UIViewController+Swizzler.h"
#import <objc/runtime.h>
#import "MNetViewControllerParser.h"
#import "MNetViewTreeTracer.h"
#import "MNetViewUtils.h"

#define SWIZZLE_PREFIX @"swizzled_"

@implementation UIViewController (Swizzler)
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        NSArray *selectorNamesToSwizzle = @[
                                            @"viewWillAppear:",
                                            @"viewWillDisappear:",
                                            @"viewDidLoad",
                                            @"viewDidAppear:",
                                        ];
        
        for(NSString *selStr in selectorNamesToSwizzle){
            NSString *swizzleSelStr = [NSString stringWithFormat:@"%@%@", SWIZZLE_PREFIX, selStr];
            
            SEL originalSel = NSSelectorFromString(selStr);
            SEL swizzleSel = NSSelectorFromString(swizzleSelStr);
            
            [MNetViewUtils swizzleMethod:originalSel
                        withSwizzlingSel:swizzleSel
                               fromClass:class];
        }
        
    });
}

#pragma mark - Method Swizzling

-(void) swizzled_viewDidLoad {
    [self swizzled_viewDidLoad];
    NSLog(@"Swizzled : view did load");
}

- (void)swizzled_viewWillAppear:(BOOL)animated {
    [self swizzled_viewWillAppear:animated];
    NSLog(@"Swizzled : view will appear");
}

-(void) swizzled_viewDidAppear:(BOOL) animated{
    [self swizzled_viewDidAppear:animated];
    NSLog(@"Swizzled : view did appear");
    if(![self isKindOfClass:[UINavigationController class]]){
     //   [[MNetViewControllerParser sharedInstance] startParsingViewController:self];
        [[MNetViewTreeTracer sharedInstance] attachToViewController:self];
    }
    
}

-(void) swizzled_viewWillDisappear:(BOOL) animated {
    [self swizzled_viewWillDisappear:animated];
    NSLog(@"Swizzled : view will disappear");
}

@end
