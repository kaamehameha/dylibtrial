//
//  MNetViewControllerParser.h
//  Pods
//
//  Created by kunal.ch on 05/05/17.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MNetViewControllerParser : NSObject

- (void)startParsingViewController:(UIViewController *)viewController;
- (void)stopParsingViewController:(UIViewController *)viewController;
+ (MNetViewControllerParser *)sharedInstance;

@end
