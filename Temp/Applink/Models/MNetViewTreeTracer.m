//
//  MNetViewTreeTracer.m
//  Pods
//
//  Created by kunal.ch on 12/05/17.
//
//

#import "MNetViewTreeTracer.h"
#import "MNetViewControllerParser.h"
#import "MNetViewTree.h"
#import "MNetViewTreeChangedEvent.h"

@interface MNetViewTreeTracer()

@property (nonatomic) MNetViewTree *viewTree;

@end

@implementation MNetViewTreeTracer

+ (MNetViewTreeTracer *)sharedInstance {
    static MNetViewTreeTracer *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if([keyPath isEqualToString:@"view"]){
        NSLog(@"changes in view observed");
        [[MNetViewControllerParser sharedInstance] startParsingViewController:_controller];
    }
}

-(void) attachToViewController:(UIViewController *)controller {
   [self refreshViewTree : (UIViewController *) controller];
}

-(void) attachObserverToViewController:(UIViewController *)controller {
    [controller addObserver:self forKeyPath:@"view" options:NSKeyValueObservingOptionInitial context:nil];
}

-(void) deattachObserverToViewController:(UIViewController *)controller {
    [controller removeObserver:self  forKeyPath:@"view"];
    
}

-(void) refreshViewTree : (UIViewController *) controller{
    MNetViewTree *viewTree = [[MNetViewTree alloc] initWithViewController:controller];
    _viewTree = viewTree;
    MNetViewTreeChangedEvent *event = [[MNetViewTreeChangedEvent alloc] init];
    NSString *link = [event getLinkForViewController:[NSStringFromClass(controller.class) lowercaseString]];
    if([viewTree getEncodedSegmentLink] != nil && ![[viewTree getEncodedSegmentLink] isEqualToString:@""]){
        link = [link stringByAppendingString:[NSString stringWithFormat:@"?%@", [viewTree getEncodedSegmentLink]]];
    }
    NSLog(@"link is %@",link);
}

@end
