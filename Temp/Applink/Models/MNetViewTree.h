//
//  MNetViewTree.h
//  Pods
//
//  Created by kunal.ch on 11/05/17.
//
//

#import <Foundation/Foundation.h>
#import "MNetViewInfo.h"
#import "MNetViewClone.h"

@interface MNetViewTree : NSObject

@property (nonatomic) NSMutableDictionary<NSString*,MNetViewClone*> *nodeMap;
@property (nonatomic) NSMutableArray<MNetViewInfo*> *clickables;
@property (nonatomic) NSMutableDictionary *jsonViewTree;
@property (nonatomic) NSString *uniqueSegmentLink;

-(instancetype) initWithViewController : (UIViewController *) controller;
-(NSString *) getEncodedSegmentLink;

@end
