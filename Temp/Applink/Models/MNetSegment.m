//
//  MNetSegment.m
//  Pods
//
//  Created by kunal.ch on 15/05/17.
//
//

#import "MNetSegment.h"
#import "MNetConstants.h"

@implementation MNetSegment

- (instancetype)initWithViewInfo:(MNetViewInfo *)viewInfo {
  self = [super init];
  if (self) {
    _resourceName = viewInfo.resourceName;
    _offset = viewInfo.startOffset;
    _viewId = viewInfo.viewId;
    _pageCount = viewInfo.pageCount;
  }
  return self;
}

- (NSMutableDictionary *)contentMap {
  NSMutableDictionary *contentObj = [[NSMutableDictionary alloc] init];
  if (_resourceName != nil) {
    [contentObj setObject:_resourceName forKey:RESOURCE_NAME];
  }
  [contentObj setObject:[NSNumber numberWithInt:_offset] forKey:START_OFFSET];
  [contentObj setObject:[NSNumber numberWithInt:_pageCount] forKey:PAGE_COUNT];
  [contentObj setObject:[NSNumber numberWithInt:_viewId] forKey:VIEW_ID];
  return contentObj;
}

- (NSString *)getContentsForSegmentAtIndex:(int)index {

  NSString *base = [NSString stringWithFormat:@"s[%d]", index];
  NSString *stringBuilder = @"";
  stringBuilder = [stringBuilder stringByAppendingString:base];
  stringBuilder = [stringBuilder stringByAppendingString:@".r="];
  stringBuilder = [stringBuilder stringByAppendingString:_resourceName];
  stringBuilder = [stringBuilder stringByAppendingString:@"&"];
  stringBuilder = [stringBuilder stringByAppendingString:base];
  stringBuilder = [stringBuilder stringByAppendingString:@".id="];
  stringBuilder = [stringBuilder
      stringByAppendingString:[NSString stringWithFormat:@"%d", _viewId]];
  stringBuilder = [stringBuilder stringByAppendingString:@"&"];
  stringBuilder = [stringBuilder stringByAppendingString:base];
  stringBuilder = [stringBuilder stringByAppendingString:@".of="];
  stringBuilder = [stringBuilder
      stringByAppendingString:[NSString stringWithFormat:@"%d", _offset]];
  return stringBuilder;
}
@end
