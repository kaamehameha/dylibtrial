//
//  MNetViewTreeTracer.h
//  Pods
//
//  Created by kunal.ch on 12/05/17.
//
//

#import <Foundation/Foundation.h>

@interface MNetViewTreeTracer : NSObject

@property(nonatomic) UIViewController *controller;

- (void)attachObserverToViewController:(UIViewController *)controller;

- (void)deattachObserverToViewController:(UIViewController *)controller;

- (void)attachToViewController:(UIViewController *)controller;

+ (MNetViewTreeTracer *)sharedInstance;

@end
