//
//  MNetViewTree.m
//  Pods
//
//  Created by kunal.ch on 11/05/17.
//
//

#import "MNetViewTree.h"
#import "MNetViewInfo.h"
#import "MNetConstants.h"
#import "MNetSegment.h"
#import "AFHTTPSessionManager.h"

@interface MNetViewTree()
@property (nonatomic) int idCounter;
@end

@implementation MNetViewTree

-(instancetype) initWithViewController : (UIViewController *) controller {
    
    self = [super init];
    
    if(self){
        _uniqueSegmentLink = @"";
        _nodeMap = [[NSMutableDictionary alloc] init];
        _jsonViewTree = [[NSMutableDictionary alloc] init];
        NSMutableArray *children = [[NSMutableArray alloc] init];
        UIView *rootView = [[[[UIApplication sharedApplication] keyWindow] rootViewController] view];
        MNetViewInfo *rootViewInfo = [[MNetViewClone create: rootView viewController:controller] viewInfo];
        _idCounter = 0;
        rootViewInfo.viewId = _idCounter;
        [self generateTree: controller.view viewInfo:rootViewInfo viewController:controller children:children];
        [_jsonViewTree setObject: [rootViewInfo json] forKey: PARENT];
        [_jsonViewTree setObject: children forKey:CHILD];
        
        if([[self getSegments] count] > 0){
            [_jsonViewTree setObject: [self getSegments] forKey:SEGMENTS];
        }
        
        NSError *error = [[NSError alloc] init];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject: _jsonViewTree options:NSJSONWritingPrettyPrinted error:&error];
        
        if(error != nil){
            NSString *jsonString = [[NSString alloc] initWithData: jsonData encoding: NSUTF8StringEncoding];
            NSLog(@"the parsed dictionary for viewController : %@ is %@",NSStringFromClass(controller.class), jsonString);
            
        
            NSString *url = @"http://172.16.61.16:5000/contents";
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            NSDictionary *parametersDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
            
            NSLog(@"INSIDE: Making the HTTP call!");
            [manager POST:url
               parameters:parametersDict
                 progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"success!");
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"error: %@", error);
            }];
        }
        
    }
    return self;
}

-(void) generateTree : (UIView *) view viewInfo : (MNetViewInfo *) parentViewInfo viewController : (UIViewController *) controller children : (NSMutableArray *) children {
   
    for (UIView *subview in view.subviews){
  
        MNetViewClone *viewClone = [MNetViewClone create: subview viewController:controller];
        MNetViewInfo *viewInfo = [viewClone viewInfo];
        
        if(parentViewInfo != nil){
            [viewInfo setParent: parentViewInfo];
        }
        _idCounter = _idCounter + 1;
        viewInfo.viewId = _idCounter;
 
        if([subview isKindOfClass:[UITableView class]] ||
           [subview isKindOfClass:[UICollectionView class]] ||
           [subview isKindOfClass:[UITabBar class]]){
            
            [_nodeMap setObject:viewClone forKey: [NSString stringWithFormat:@"%d",[viewInfo viewId]]];
        /*
            MNetViewClone *innerViewClone = [MNetViewClone create: innerView viewController:controller];
            MNetViewInfo *innerViewInfo = [innerViewClone viewInfo];
            
            _idCounter = _idCounter + 1;
            innerViewInfo.viewId = _idCounter;
            [innerViewInfo setParent: [viewClone viewInfo]];
            NSMutableArray *innerChildren = [[NSMutableArray alloc] init];
            NSMutableDictionary *innerJson = [innerViewInfo json];
            
            [_nodeMap setObject:viewClone forKey: [NSString stringWithFormat:@"%d",[innerViewInfo viewId]]];
            
            [self generateTree:nil viewInfo: innerViewInfo viewController:controller children:innerChildren];
            [innerJson setObject:innerChildren forKey:CHILD];
            [parentJson setObject: [NSMutableArray arrayWithObject: innerChildren] forKey: CHILD];

             */
        }else{
            if([subview.subviews count] > 0){
                NSMutableDictionary *parentJson = [viewInfo json];
                NSMutableArray *childJson =  [[NSMutableArray alloc] init];
                
                for(UIView *innerView in subview.subviews){

                    [self generateTree: innerView viewInfo: viewInfo viewController: controller children:childJson];
                    [parentJson setObject: childJson forKey:CHILD];

                }
                [children addObject:parentJson];
            }else{
                [children addObject:[viewInfo json]];
            }
        }
    }
}

-(NSMutableArray *) getSegments {
    _uniqueSegmentLink = @"";
    NSMutableArray *segments = [[NSMutableArray alloc] init];
    int i = 0;
    int count = (int)[_nodeMap count];
    
    for(NSString *key in _nodeMap){
        MNetViewClone *viewClone = [_nodeMap valueForKey:key];
        MNetSegment *segment = [[MNetSegment alloc] initWithViewInfo:[viewClone viewInfo]];
        [segments addObject:[segment contentMap]];
        
        NSString *segmentLink = @"";
        
        if(i != (count - 1)){
            segmentLink = [NSString stringWithFormat:@"%@&",[segment getContentsForSegmentAtIndex:i]];
        }else{
            segmentLink = [NSString stringWithFormat:@"%@",[segment getContentsForSegmentAtIndex:i]];
        }
        
        _uniqueSegmentLink = [_uniqueSegmentLink stringByAppendingString:segmentLink];
        
        i++;
    }
    return segments;
}

-(NSString *) getEncodedSegmentLink {
    NSString *encodedSegmentLink = @"";
    if(_uniqueSegmentLink != nil && ![_uniqueSegmentLink isEqualToString:@""]){
        NSCharacterSet *characterSet = [NSCharacterSet URLHostAllowedCharacterSet];
        encodedSegmentLink =[_uniqueSegmentLink stringByAddingPercentEncodingWithAllowedCharacters: characterSet];
    }
    return encodedSegmentLink;
}

@end
