//
//  MNetViewClone.h
//  Pods
//
//  Created by kunal.ch on 11/05/17.
//
//

#import <Foundation/Foundation.h>
#import "MNetViewInfo.h"

@interface MNetViewClone : NSObject

@property(nonatomic) MNetViewInfo *viewInfo;

@property(nonatomic) NSMutableDictionary<NSString*,NSObject*> *properties;

+(MNetViewClone *) create : (UIView *) view viewController : (UIViewController *) controller;

@end
