//
//  MNetViewClone.m
//  Pods
//
//  Created by kunal.ch on 11/05/17.
//
//

#import "MNetViewClone.h"
#import "MNetViewUtils.h"

static NSString *TEXT = @"text";
static NSString *IS_ADAPTER_CHILD = @"is_adapter_child";

@implementation MNetViewClone

+ (MNetViewClone *)create:(UIView *)view
           viewController:(UIViewController *)controller {

  MNetViewClone *clone = [[MNetViewClone alloc] init];
  clone.properties = [[NSMutableDictionary alloc] init];

  [clone setViewInfo:[[MNetViewInfo alloc] initWithView:view
                                         viewController:controller]];

  if ([view isKindOfClass:[UITextView class]]) {
    UITextView *textView = (UITextView *)view;
    if (textView.text != nil) {
      [clone.properties setObject:textView.text forKey:TEXT];
    }

  } else if ([view isKindOfClass:[UILabel class]]) {

    UILabel *label = (UILabel *)view;
    if (label.text != nil) {
      [clone.properties setObject:label.text forKey:TEXT];
    }

  } else if ([view isKindOfClass:[UITabBar class]]) {

    UITabBar *tabBar = (UITabBar *)view;
    [clone viewInfo].pageCount = (int)[[tabBar items] count];

  } else if ([view isKindOfClass:[UITableView class]]) {

    int totalCount = 0;
    int pageCount = 0;
    UITableView *tableView = (UITableView *)view;

    totalCount = (int)[[tableView visibleCells] count];

    if (totalCount != 0) {
      [tableView setTag:totalCount];
      pageCount = (int)[tableView tag];
    }
    /*
        Will retun array of NSIndexPath of visible cells
     */
    NSArray *indexPathsForVisibleRows = [tableView indexPathsForVisibleRows];

    NSIndexPath *firstVisibleIndexPath = indexPathsForVisibleRows[0];

    [clone viewInfo].startOffset = (int)[firstVisibleIndexPath row];

    [clone viewInfo].pageCount = pageCount;

  } else if ([view isKindOfClass:[UICollectionView class]]) {

    int totalCount = 0;
    int pageCount = 0;
    UICollectionView *collectionView = (UICollectionView *)view;

    totalCount = (int)[[collectionView visibleCells] count];

    if (totalCount != 0) {
      [collectionView setTag:totalCount];
      pageCount = (int)[collectionView tag];
    }

    /*
      will return array of NSIndexPath of visible cells
     */

    NSArray *indexPathsForVisibleRow =
        [collectionView indexPathsForVisibleItems];

    NSIndexPath *firstVisibleIndexPath = indexPathsForVisibleRow[0];

    [clone viewInfo].startOffset = (int)[firstVisibleIndexPath row];

    [clone viewInfo].pageCount = pageCount;
  }

  [clone.properties
      setObject:[NSNumber
                    numberWithBool:[MNetViewUtils isAdapterChild:view
                                                  viewController:controller]]
         forKey:IS_ADAPTER_CHILD];

  [[clone viewInfo] setProperties:[clone properties]];

  return clone;
}

@end
