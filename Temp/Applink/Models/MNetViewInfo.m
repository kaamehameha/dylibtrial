//
//  MNetViewInfo.m
//  Pods
//
//  Created by kunal.ch on 11/05/17.
//
//

#import "MNetViewInfo.h"
#import "MNetConstants.h"
#import "MNetViewUtils.h"

@interface MNetViewInfo ()
@property(nonatomic) UIViewController *viewController;
@end

@implementation MNetViewInfo

- (instancetype)initWithView:(UIView *)view
              viewController:(UIViewController *)controller {
  self = [super init];
  if (self) {
    _view = [[UIView alloc] init];
    _view = view;
    _viewController = controller;
    _viewClass = NSStringFromClass(controller.class);
    [self processViewForDetails:controller.view];
  }
  return self;
}

- (void)processViewForDetails:(UIView *)view {

  if ([view isKindOfClass:[UITableView class]] ||

      [view isKindOfClass:[UICollectionView class]] ||

      [view isKindOfClass:[UIScrollView class]]) {

    _isScrollable = YES;
  }

  if ([view isKindOfClass:[UIScrollView class]]) {
    _isClickable = NO;
  }
  _viewClass = NSStringFromClass(view.class);
  _viewType = NSStringFromClass(view.superclass);
  _resourceName =
      [MNetViewUtils getResourceNameForView:view withId:(int)_viewId];
}

- (NSMutableDictionary *)json {
  NSMutableDictionary *jsonObject = [[NSMutableDictionary alloc] init];

  [jsonObject setObject:[NSNumber numberWithInt:_viewId] forKey:VIEW_ID];
  [jsonObject setObject:_viewClass forKey:VIEW_CLASS];
  [jsonObject setObject:_viewType forKey:VIEW_TYPE];
  [jsonObject setObject:[NSNumber numberWithBool:_isScrollable]
                 forKey:SCROLLABLE];
  [jsonObject setObject:[NSNumber numberWithBool:_isClickable]
                 forKey:CLICKABLE];
  [jsonObject setObject:_properties forKey:PROPERTIES];
  [jsonObject setObject:_resourceName forKey:RESOURCE_NAME];

  if (_parent != nil) {
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:_viewClass forKey:VIEW_CLASS];
    [parent setObject:_viewType forKey:VIEW_TYPE];
    [parent setObject:[NSNumber numberWithInt:[_parent viewId]] forKey:VIEW_ID];
    [parent setObject:_resourceName forKey:RESOURCE_NAME];
    [jsonObject setObject:parent forKey:PARENT];
  }

  return jsonObject;
}

@end
