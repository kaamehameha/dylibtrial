Pod::Spec.new do |s|
  s.name = "Temp"
  s.version = "0.1.0"
  s.summary = "A short description of Temp."
  s.license = {"type"=>"MIT", "file"=>"LICENSE"}
  s.authors = {"Nithin"=>"nithin.linkin@gmail.com"}
  s.homepage = "https://github.com/Nithin/Temp"
  s.description = "TODO: Add long description of the pod here."
  s.frameworks = ["SystemConfiguration", "WebKit", "MobileCoreServices"]
  s.source = { :path => '.' }

  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'ios/Temp.framework'
end
