# Temp

[![CI Status](http://img.shields.io/travis/Nithin/Temp.svg?style=flat)](https://travis-ci.org/Nithin/Temp)
[![Version](https://img.shields.io/cocoapods/v/Temp.svg?style=flat)](http://cocoapods.org/pods/Temp)
[![License](https://img.shields.io/cocoapods/l/Temp.svg?style=flat)](http://cocoapods.org/pods/Temp)
[![Platform](https://img.shields.io/cocoapods/p/Temp.svg?style=flat)](http://cocoapods.org/pods/Temp)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Temp is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Temp"
```

## Author

Nithin, nithin.linkin@gmail.com

## License

Temp is available under the MIT license. See the LICENSE file for more info.
