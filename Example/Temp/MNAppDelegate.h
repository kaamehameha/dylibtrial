//
//  MNAppDelegate.h
//  Temp
//
//  Created by Nithin on 02/14/2017.
//  Copyright (c) 2017 Nithin. All rights reserved.
//

@import UIKit;

@interface MNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
