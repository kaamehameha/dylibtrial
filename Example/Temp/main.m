//
//  main.m
//  Temp
//
//  Created by Nithin on 02/14/2017.
//  Copyright (c) 2017 Nithin. All rights reserved.
//

@import UIKit;
#import "MNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MNAppDelegate class]));
    }
}
